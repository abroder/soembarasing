# so embarasing

@[soembarasing](http://twitter.com/soembarasing)

I have a lot of trouble spelling the word "embarrassing," so I wanted 
validation that I wasn't the only one. Originally this retweeted every tweet
that contained a misspelling of embarrassing that had only one "r" or one "s"
(or both), but that was incredibly high noise, so I made it favorite every
tweet that matched the criteria, and then retweet a random tweet from the last
30 minutes every half hour.

It eventually got banned from writing to the Twitter API, so turns out it was
still too high noise.
