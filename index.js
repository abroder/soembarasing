var Twit = require('twit')
var Twitter = require('../Twitter')
require('dotenv').load()

var T = new Twit({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
})
var twitter = new Twitter(T)

var tweets = []

var retweet = function (tweet_id) {
  T.post('statuses/retweet/:id', { id: tweet_id }, function (err, data, res) {
    if (err) {
      console.error(`${ err } (${ tweet_id })`)
      return
    }

    console.log(`Successfully retweeted: ${ data.text }`)
  })
}

var favorite = function (tweet_id) {
  T.post('favorites/create', { id: tweet_id }, function (err, data, res) {
    if (err) {
      console.error(`${ err } (${ tweet_id })`)
      return
    }

    console.log(`Successfully favorited: ${ data.text }`)
  })
}

var follow = function (user_id) {
  T.post('friendships/create', { id: user_id }, function (err, data, res) {
    if (err) {
      console.error(`${ err } (${ user_id })`)
      return
    }

    console.log(`Successfully followed: @${ data.screen_name }`)
  })
}

setInterval(() => {
  if (tweets.length == 0) {
    return
  }

  var tweet = tweets[Math.floor(Math.random() * tweets.length)]

  retweet(tweet.id_str)

  var retweeted_tweet = 
    (tweet.retweeted_status !== undefined)

  if (!tweet.user.following)
    follow(tweet.user.id_str)
  if (retweeted_tweet && !tweet.retweeted_status.user.following)
    follow(tweet.retweeted_status.user.id_str)

  tweets = []
}, 1000 /* milliseconds */ * 60 /* seconds */ * 30 /* minutes */)

var stream = T.stream('statuses/filter', { track: 'embarasing,embarrasing,embarassing' })
stream.on('tweet', function (tweet) {
  console.log(`Received tweet ${ tweet.id_str }.`)
  var retweeted_tweet = 
    (tweet.retweeted_status !== undefined)

  /* filter out bot's tweets */
  if (tweet.user.screen_name === 'soembarasing' ||
    (retweeted_tweet && tweet.retweeted_status.user.screen_name === 'soembarasing'))
    return

  /* filter out sensitive content */
  var sensitive = (tweet.possibly_sensitive || 
    (retweeted_tweet && tweet.retweeted_status.possibly_sensitive))
  if (sensitive)
    return

  /* don't attempt to retweet a tweet that's already been retweeted */
  if (tweet.retweeted ||
     (retweeted_tweet && tweet.retweeted_status.retweeted))
    return
  
  favorite(tweet.id_str)
  tweets.unshift(tweet)
})

console.log('Listening for tweets.')
